# Warp10
Fasar
2020-12-12
:jbake-type: post
:jbake-tags: bigdata
:jbake-status: published


## Test des performances de lecture ecriture

* Insertion de 31536000 elements
** Utilise environ 520 Mo de place : 16,5 octets par elements
** Avec des batchs d'inserts de 200 :  environ  273_664 inserts / seconde.
** Avec des batchs d'inserts de 3600 : environ  287_296 inserts / seconde.

* Selection des 31536000 elements:
** Lecture de flux sur http streams
** Lecture de environ : 541_716 elements / secondes.
** Moins de une minute


J'ai créer un repo github avec mes tests en Warp10 : https://github.com/fasar/warp10-test

## Taille des données

Lancement de Warp10 avec docker: `docker run -it --rm -v $PWD/w10-data/:/data/ warp10io/warp10:2.7.2-ci bash`

Lancement de Warp10 : warp10.start.sh.
Il faut pousser des données dans le warp10.

Lancement d'une compaction  : `/usr/lib/jvm/java-1.8-openjdk/jre/bin/java -cp /opt/warp10/bin/warp10-2.7.2.jar -Dfile.encoding=UTF-8 io.warp10.standalone.WarpCompact /data/warp10/leveldb "" ""`

* une time serie de 1 an de données avec un échantillonnage de 1 seconde demande environ 438 Mo

* les levels de la sstable sont divisées en fichiers de 2,1 Mo. 


## Delete des données : 

SERVER=172.17.0.2:8080
curl -v \
    -H 'X-Warp10-Token: writeTokenCI' "http://$SERVER/api/v0/delete?selector=sensor%7B%7D&start=1970-01-01T00:00:05Z&end=1970-12-02T00:00:05Z"

"sensor"

## Autres:

Les drivers :

* Pas de drivers
* Les valeur sont données en flux a partir du service /api/v0/fetch. Il n'y a pas de limite de stream.
* Il est possible de travailler sur une partie des données grace au service /api/v0/exec

## Leveldb

* LevelDb est un moteur clef -> valeur mature.
* Il n'y a plus trop de développement autour de ce moteur : https://github.com/google/leveldb


* Les dependences de leveldb datent :
** Le moteur leveldb en JNI : org.fusesource.leveldbjni:leveldbjni:1.8 date de 2013
** Le moteur leveldb en Java Natif : org.iq80.leveldb:leveldb:0.12 date de 2019


* Explication du mécanisme :
** https://blog.senx.io/demystifying-leveldb/
** https://github.com/google/leveldb/blob/master/doc/impl.md



## WarpScript

Bench:

* Pour executer un script:
** Requête de 300 ms pour récupérer 2 jours de données
** Requête de 439 ms pour faire une query de 2 jours avec un resampling a 1 seconde
** Requête de 491 ms pour faire une query de 2 jours avec un resampling a 1 seconde et un MAP qui filtre les valeurs au dela de 20.0 Volt
** Requête de 533 secondes pour faire une query de 2 jours avec un resampling a 1 seconde puis un MAP qui filtre les valeurs au delà de 20.0 Volt et un MAP qui filtre les valeurs en dessous de 50V

* Avec les même donnée sous Cassandra et SCMS:
** Il faut PT0.6S pour aller chercher les données de deux jours
** Il faut PT0.731S aller chercher les données de deux jours avec un resampling a 1 seconde.
** Il faut PT0.732S aller chercher les données de deux jours avec un resampling a 1 seconde et deux filter des données pour garder les valeurs au delà de 20V et en dessous de 50V.


Bench Internet:

* Dans la video : https://www.youtube.com/watch?v=6kFv_W6Y-Q0&feature=emb_logo , a 1h02, le présentateur dit que Warp10 peut lire 500_000 mesure par seconde sur des disques dur classique. Donc il faut 63 secondes pour lire 1 année de données.


## Quelques informations sur Warp10

* https://www.cerenit.fr/tags/warpscript/
** Durée dans pendant lequel un équipement est au-dela d'un certain seuil: https://www.cerenit.fr/blog/timeseries-state-duration/

* Rendre image on server side with WarpScript : https://blog.senx.io/drawing-server-side-part-1/

* https://blog.senx.io/tag/extreme-warp-10/
** Checher de UFO
** Les chemtray

## Tests à faire :

* Supprimer des données profonde dans leveldb. Les suppressions sont écritent dans les level les plus haut. Donc necessaire de consolider.
* Comment dimentionner l'éspace necessaire pour x timeseries sauvegarder y ans.
