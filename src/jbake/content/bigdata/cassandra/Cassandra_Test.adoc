# Cassandra
Fasar
2021-01-14
:jbake-type: page
:jbake-tags: cassandra
:jbake-status: published


Test des performances d'insert pour un model de donnée [(ts, sensor_name), value] avec -Xms=2G de Ram

* Insertion de 31536000 elements
** Utilise environ 400 Mo de place : 12 octets par elements
** Avec des batchs d'inserts de 100 : environ 171_937 inserts / seconde.
** Avec des batchs d'inserts de 3600 : environ 252_937 inserts / seconde.

* Selection des 31536000 elements:
** Lecture en boucle sur le ResultSet
** Lecture de environ : 560_839 elements / secondes.
** (moins de 60 secondes)

## Model de données Cassandra

[source,sql]
----
CREATE KEYSPACE IF NOT EXISTS db_test WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'}  AND durable_writes = true;

CREATE TABLE db_test.tag (
    name text,
    ts timestamp,
    value double,
    PRIMARY KEY (name, ts)
) WITH CLUSTERING ORDER BY (ts DESC);
----

## Cassandra et les tables

* Impacts of many tables in a Cassandra data model :  https://thelastpickle.com/blog/2020/11/25/impacts-of-many-tables-on-cassandra.html

* Tomstone pb : https://medium.com/de-bijenkorf-techblog/experiences-with-tombstones-in-apache-cassandra-7302092e7423

