= Raspberry Pi QEmu
Fasar
2019-09-08
:jbake-type: page
:jbake-tags: documentation, manual
:jbake-status: published

First of all, If you want to run a raspberry pi with qemu, you should know: It is verrryyy sssllooowwww.....

I've got a i7

And my emulated raspy is :

```console
pi@raspberrypi:~ $ cat /proc/cpuinfo | grep MIPS
BogoMIPS        : 554.59
```

On normal Raspy 3, I have :
```console
pi@raspberrypi:~ $ cat /proc/cpuinfo | grep MIPS
BogoMIPS        : 38.40
BogoMIPS        : 38.40
BogoMIPS        : 38.40
BogoMIPS        : 38.40
```



== Raspberry Pi QEmu installation

* How to emulate a Raspberry Pi with QEmu

* Source of this documentation : https://azeria-labs.com/emulate-raspberry-pi-with-qemu/
* French : https://www.supinfo.com/articles/single/5429-emuler-une-raspberry-pi-linux-avec-qemu
* QEmu options: https://wiki.gentoo.org/wiki/QEMU/Options

* Download the last version of Raspbian : http://downloads.raspberrypi.org/raspbian/images/raspbian-2019-07-12/
* Download the kernel version of Raspberry : https://github.com/dhruvvyas90/qemu-rpi-kernel

* In the file /etc/fstab of the image:
** Comment out all commented line (line start with #)
** Change all the mount point from mmcblk0 

* Activate ssh :
** sudo update-rc.d ssh enable
** sudo raspi-config

== Resizing the Raspbian image

* Close qemu and run on host :

```console
$ qemu-img resize raspbian.img +6G
```

* I think you can do that with:

```console
$ dd if=/dev/zero bs=1M count=6000 status=progress >> raspbian.img 
```

Method 1 - with Qemu::

* Run qemu and resize with `$ sudo cfdisk /dev/sda`
* Let know the system of the new size: `sudo resize2fs /dev/sda2`
* Restart the qemu : `sudo shutdown -r now`

Method 1 - Directly on image::

* Resize partition with `$ sudo cfdisk raspbian.img`
* Mount the partition on a loop device `sudo losetup  -f --show -o $((532480*512))  2020-02-13-raspbian-buster-lite.img` 
* Check all is all right `sudo e2fsck -f /dev/loopXX`
* Resize the file system `$ sudo resize2fs /dev/loopXX`




== Raspberry Pi QEmu on Windows OS

* To change fstab on windows simply use a 7-Zip app

* Install windows QEmu : https://www.qemu.org/

```console
$ qemu-system-arm -M versatilepb  -cpu arm1176 -m 256 \
   -drive file=2019-07-10-raspbian-buster-lite.img,format=raw  \
   -net nic -net user,hostfwd=tcp::5022-:22 -dtb versatile-pb.dtb \
   -kernel kernel-qemu-4.19.50-buster  \
   -serial stdio  \
   --append "root=PARTUUID=3b18e43a-02 rootfstype=ext4  rootwait"
```

With 3b18e43a, the Disk identifier you get with the command : `fdisk -l 2019-07-10-raspbian-buster.img`

== Raspberry Pi QEmu Linux

* Install QEmu on your system : `sudo apt-get install qemu-system qemu-system-arm`

* To change fstab on linux you can mount the image on a mount point:
** check the offset of the data part of the image with `fdisk -l 2019-07-10-raspbian-buster.img`
** The start element should be multiply with the number of bytes by sector.
** For example, the following result of the image is 540672 * 512 = 276824064.
** The fdisk output is:

```console
$ fdisk -l 2019-07-10-raspbian-buster.img
Disk 2019-07-10-raspbian-buster.img: 3.5 GiB, 3779067904 bytes, 7380992 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x3b18e43a

Device                          Boot  Start     End Sectors  Size Id Type
2019-07-10-raspbian-buster.img1        8192  532480  524289  256M  c W95 FAT32 (LBA)
2019-07-10-raspbian-buster.img2      540672 7380991 6840320  3.3G 83 Linux
```

** Now, you can mount the image

```console
$ sudo mkdir /mnt/raspbian
$ sudo mount -v -o offset=276824064 -t ext4 ~/qemu_vms/<your-img-file.img> /mnt/raspbian
```

** Comment the first line of /mnt/raspbian/etc/ld.so.preload


** Edit the fstab  with : `$ sudo nano /mnt/raspbian/etc/fstab`
** uncomment all commented lines 
** replace /dev/mmcblk0p1 with /dev/sda1 and  /dev/mmcblk0p2 with /dev/sda2
   

* Now you can emulate it on Qemu by using the following command:
** Try the same command line as windows


=== No Edit fstab

* Check : https://unix.stackexchange.com/questions/243515/why-cant-the-kernel-run-init
*

== Qu'est que DTS/DTB

Source: https://linuxfr.org/forums/linux-embarque/posts/fichiers-dtb-ramdisk-noyau

"Device Tree Source" est un fichier qui va décrire ta board et ses périphériques.
Ce fichier réunit les informations "bas niveau" sur l'organisation des bus, les interrputions, des mapping mémoire, etc.
Il se compile donc en dtb. 
Ça permet de ne pas coder "en dur" l'initialisation des périphériques dans le kernel.
Je suppose qu'on peut comme ça utiliser le même kernel sur deux boards différentes (mais de la même archi, quand même) en changeant juste le dtb.

Initramfs (anciennement initrd): 

* Il contient les modules nécessaires au boot de ta carte. Cela permet de mettre plein de drivers dedans pour plein de périphs différents, mais qui ne seront chargés qu'en fonction de leur utilisation.

Comment faire un DTS/DTB : https://www.framboise314.fr/un-point-sur-le-device-tree/