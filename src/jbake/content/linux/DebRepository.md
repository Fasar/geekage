# Créer son propre repository pour Debian ou Ubuntu
fasar
2023-02-02
:jbake-type: post
:jbake-tags: linux
:jbake-status: Draft
:idprefix:


## Créer son propre repository pour Debian ou Ubuntu

Pour créer son propre repository pour Debian ou Ubuntu, il faut commencer par créer une arborescence de fichiers qui ressemblera à ceci:

```bash
.
├── conf
│   ├── distribution
│   └── signing-key
├── dists
│   └── stable
│       ├── main
│       │   ├── binary-amd64
│       │   │   ├── Packages
│       │   │   └── Packages.gz
│       │   ├── binary-i386
│       │   │   ├── Packages
│       │   │   └── Packages.gz
│       │   ├── source
│       │   │   ├── Sources
│       │   │   └── Sources.gz
│       │   └── Translation-fr
│       │       ├── Translation
│       │       └── Translation.gz
│       └── Release
├── pool
│   ├── main
│   │   ├── a
│   │   │   └── apache2
│   │   │       └── apache2_2.2.22-1_amd64.deb
│   │   └── l
│   │       └── libapache2-mod-php5
│   │           └── libapache2-mod-php5_5.4.4-14+deb.sury.org~precise+1_amd64.deb
│   └── source
│       └── a
│           └── apache2
│               └── apache2_2.2.22-1.dsc
└── project
```

Le fichier `conf/distribution` contient les informations sur votre distribution:

```text
Origin: Fasar
Label: Fasar
Suite: stable
Version: 1.0
Codename: stable
Architectures: i386 amd64 source
Components: main
Description: Fasar's Debian repository
```

Le fichier `conf/signing-key` contient le nom de votre clé de signature.
Il faut créer une clé de signature avec `gpg --gen-key` et ensuite copier le nom de la clé dans ce fichier.


## Ajout d'un paquet

Nous allons maintenant ajouter un paquet dans le repository. 
Pour cela, il faut copier le fichier `.deb` dans le répertoire `pool/main/binary-amd64` et de lancer la commande suivante :

`dpkg-scanpackages pool/main/binary-amd64 /dev/null > dists/debian/Release/Packages`

## Configuration du serveur web

Utiliser http-server pour servir le repository:

- `http-server -p 80 -c-1 -a`


## Utilisation de votre repository

Pour utiliser votre repository, il faut ajouter le fichier suivant dans le répertoire `/etc/apt/sources.list.d/`:

- deb http://repo.fasar.net stable main

Puis, il faut installer la clé de signature avec la commande suivante:

- `wget -O - http://repo.fasar.net/conf/signing-key | apt-key add -`

Et voilà, votre repository est prêt à être utilisé.

## Création de paquet Debian

Pour créer un paquet Debian, il faut commencer par créer un fichier `control` qui contient les informations sur le paquet:

```text
Package: my-package
Version: 1.0.0-1
Architecture: amd64
Maintainer: Fasar
Description: My package
```

Ensuite, il faut créer un fichier `postinst` qui contient les commandes à exécuter après l'installation du paquet:

```bash
#!/bin/sh
echo "Hello world"
```

Et enfin, il faut créer un fichier `changelog` qui contient les informations sur les versions du paquet:

```text
my-package (1.0.0-1) stable; urgency=low

  * First release.

 -- Fasar <
```

Pour créer le paquet, il faut exécuter la commande suivante:

`dpkg-deb --build my-package`

Et voilà, votre paquet est prêt à être installé:

`dpkg -i my-package.deb`

