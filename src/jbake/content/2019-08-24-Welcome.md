title=Welcome Blog
date=2019-08-25
type=page
tags=update
status=draft
~~~~~~

Blog Start.

My personnal Geekage.

You’ll find this post in your `content/blog` directory. 
Go ahead and edit it and re-build the site to see your changes. You can rebuild the site in many different ways, but the most common way is to run `jbake -s`, which launches a web server and auto-regenerates your site when a file is updated.

- You can reach the web site on [http://localhost:8820](http://localhost:8820)

To add new posts, simply add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.ext` and includes the necessary front matter. Take a look at the source for this post to get an idea about how it works.
