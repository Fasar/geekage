date=2023-01-18
type=page
tags=ddd
status=published
--------

# Avantages et inconvénients

Source: https://www.baeldung.com/cqrs-event-sourcing-java



## Benefits that CQRS brings to an application architecture:

* CQRS provides us a convenient way to select separate domain models appropriate for write and read operations; we don't have to create a complex domain model supporting both
* It helps us to select repositories that are individually suited for handling the complexities of the read and write operations, like high throughput for writing and low latency for reading
* It naturally complements event-based programming models in a distributed architecture by providing a separation of concerns as well as simpler 


## Inconvénients du CQRS

CQRS adds considerable complexity to the architecture. It may not be suitable or worth the pain in many scenarios:

* Only a complex domain model can benefit from the added complexity of this pattern; a simple domain model can be managed without all this
* Naturally leads to code duplication to some extent, which is an acceptable evil compared to the gain it leads us to; however, individual judgment is advised
* Separate repositories lead to problems of consistency, and it's difficult to keep the write and read repositories in perfect sync always; we often have to settle for eventual consistency

