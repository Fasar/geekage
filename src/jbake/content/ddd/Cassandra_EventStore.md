date=2024-05-28
type=post
tags=ddd,cassandra
status=published
--------

# Cassandra comme event store

**Qu'est que l'event store:**

L'event store est un mécanisme de stockage d'événements, qui permet de stocker l'historique des événements qui ont eu lieu dans un système.
Il est utilisé dans les architectures CQRS et Event Sourcing.

Un event store permet de stocker les événements de manière séquentielle, et de les lire de manière séquentielle.
Un event dispose d'une date de création, et d'un payload qui contient les données de l'événement et d'une clé.
La clé est un identifiant unique de l'événement, qui permet de garantir l'unicité de l'événement.

Il existe un champ version qui permet de garantir l'ordre des événements.
Lorsqu'un événement est stocké, il est associé à une version, qui est incrémentée à chaque événement stocké.

Un événement est créé par un agrégat.
Il peut être généré spontanément par comme un tick d'horlogue par example.
Ou suite à une décision sur le domaine.

Dans le second cas, l'événement généré à un version incrémenté par rapport à l'événement précédent.

**Qu'est que Cassandra:**

Cassandra est une base de données distribuée, orientée colonnes, qui permet de stocker des données de manière très performante.
Elle est utilisée par de nombreux acteurs du web, comme Netflix, eBay, ou encore Twitter.

Une particualrité est qu'elle permet de stocker des données sur un cluster en réglant la cohérence des données versus la disponbilité de la base de données.
C'est à dire que soit l'on a une cohérence forte mais on est moins résilient aux pannes (réseaux, serveurs), soit l'on a une cohérence plus faible mais on est plus résilient aux pannes.

Sur un cluster Cassandra, la clé de partition permet de distribuer les données sur les différents noeuds du cluster.
Les données sont ainsi groupé par clé de partition, et chaque noeud du cluster contient un ensemble de clés de partition.

**Clé pour utiliser Cassandra comme event store:**

Les événements sont stockés avec une version.
Il y a des règles qui régissent la version des événements.
Par example, il n'est pas possible d'écrire un événement avec une version inférieure ou égale à ce qui existe déjà dans la base de données.

Les écriture doivents être réalisées de manière atomique.
C'est à dire que l'on ne peut pas écrire un événement si un autre événement a été écrit entre temps.
L'atomicité dans Cassandra est garantie avec les transactions légères uniquement sur une partitions.

Il est donc important de bien modéliser le problème.

**Modélisation des événements:**

Les événements sont stockés dans une table Cassandra.
La clé de partition est l'identifiant de l'agrégat plus une date.
La clé de cluster est la version de l'événement.

On peut ajouter dans la clé de partition un element qui indique une temporalité de l'événement.
Cela permettra de rendre les evenements compatible avec une gestion en time series et corresponds parfaitement à ce que l'on s'attend des événements métiers: qu'ils arrivent dans l'ordre !

Pour inserer un événement, il faut vérifier que la version de l'événement est supérieure à la version actuelle.
Si ce n'est pas le cas, il faut renvoyer une erreur.

En CQL, on peut utiliser un champ de valeur static à la partition pour stocker la version courante de l'agrégat.
Cela permet de garantir que la version de l'événement est supérieure à la version actuelle.
Le service permettant de persister l'evenement devra alors incrémenter la version de l'agrégat et faire la requete qui validera la verification.

Nous avons donc ces exigences qui sont réglées avec la stack suivante :

- CASSANDRA: Les événements sont stocké en timeseries
- MODEL: Ils disposent d'une version
- APPLICATION: La version est incrémenté à chaque événement
- APPLICATION: Il n'est pas possible d'écrire un événement avec une version inférieure ou égale à ce qui existe déjà dans la base de données

Le modèle de données est donc celui-ci:

```sql
DROP TABLE IF EXISTS events;
CREATE TABLE events (
    aggregate_id text,
    year_month text,
    cversion bigint static,
    iversion bigint,
    event_name text,
    jdata text,
    PRIMARY KEY ((aggregate_id, year_month), iversion)
);
```

Et la mise en place de la règle d'écriture atomique est la suivante:

```sql
UPDATE events 
    SET cversion = 1, event_name = 'eventName', jdata = 'data'
    WHERE aggregate_id = 'aggregateId' AND year_month = '2024-05' AND iversion = 1
    IF cversion = NULL;

-- OR if already exists

UPDATE events 
    SET cversion = 2, event_name = 'eventName', jdata = 'data'
    WHERE aggregate_id = 'aggregateId' AND year_month = '2024-05' AND iversion = 2
    IF cversion = 1;

```


**Exigence de cohérence:**

Afin de garantire l'atomicité des opérations mais aussi de proposé un service tollérent aux pannes, il faut indiquer que les requètes de lectures et écritures doivent être réalisées en QUORUM.

