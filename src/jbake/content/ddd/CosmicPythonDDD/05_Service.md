# Service

- Utilise le domaine, uow, 



* Express your service layer in terms of primitives rather than domain objects.

* In an ideal world, you’ll have all the services you need to be able to test entirely against the service layer, rather than hacking state via repositories or the database. This pays off in your end-to-end tests as well.




## Warp-up

**PLUS:**

* We have a single place to capture all the use cases for our application.

* We’ve placed our clever domain logic behind an API, which leaves us free to refactor.

* We have cleanly separated "stuff that talks HTTP" from "stuff that talks allocation."

* When combined with the Repository pattern and FakeRepository, we have a nice way of writing tests at a higher level than the domain layer; we can test more of our workflow without needing to use integration tests (read on to [chapter_05_high_gear_low_gear] for more elaboration on this).

**MINUS:**

* Putting too much logic into the service layer can lead to the Anemic Domain antipattern. It’s better to introduce this layer after you spot orchestration logic creeping into your controllers.

* You can get a lot of the benefits that come from having rich domain models by simply pushing logic out of your controllers and down to the model layer, without needing to add an extra layer in between (aka "fat models, thin controllers").