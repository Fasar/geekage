# External Event Bus

- Temporal Decoupling Using Asynchronous Messaging
- We should think in terms of verbs, not nouns : Our domain model is about modeling a business process. It’s not a static data model about a thing; it’s a model of a verb.
    - instead of thinking about a system for orders and a system for batches, we think about a system for ordering and a system for allocating



## Wrap-Up

Events can come from the outside, but they can also be published externally—​our publish handler converts an event to a message on a Redis channel. We use events to talk to the outside world. This kind of temporal decoupling buys us a lot of flexibility in our application integrations, but as always, it comes at a cost.

> Event notification is nice because it implies a low level of coupling, and is pretty simple to set up. 
> It can become problematic, however, if there really is a logical flow that runs over various event notifications...
> It can be hard to see such a flow as it's not explicit in any program text....
> This can make it hard to debug and modify.
> 
> Martin Fowler, "What do you mean by 'Event-Driven'"

Event-based microservices integration: the trade-offs shows some trade-offs to think about.


**Pros**

Avoids the distributed big ball of mud.

Services are decoupled: it’s easier to change individual services and add new ones.

**Cons**

The overall flows of information are harder to see.

Eventual consistency is a new concept to deal with.

Message reliability and choices around at-least-once versus at-most-once delivery need thinking through.

