# Events and Commands

* Commands capture intent
  * We name commands with imperative mood verb phrases like "allocate stock" or "delay shipment.
  * Commands are sent by one actor to another specific actor with the expectation that a particular thing will happen as a result. 
  * When we post a form to an API handler, we are sending a command. 
  

* Events are things that have happened in the past.
  * We name events with past-tense verb phrases like "stock allocated" or "shipment delayed."
  * Events are broadcast by an actor to all interested listeners. 
  * When we publish BatchQuantityChanged, we don’t know who’s going to pick it up. We name events with past-tense verb phrases like "order allocated to stock" or "shipment delayed."
  
We often use events to spread the knowledge about successful commands.

| -                  | Event               | Command         |
| ------------------ | ------------------- | --------------- |
| **Named**          | Past tense          | Imperative mood |
| **Error handling** | Fail independently. | Fail noisily    |
| **Sent to**        | All listeners       | One recipient   |


## Warp-up

In this book we decided to introduce the concept of events before the concept of commands, but other guides often do it the other way around. Making explicit the requests that our system can respond to by giving them a name and their own data structure is quite a fundamental thing to do. You’ll sometimes see people use the name Command Handler pattern to describe what we’re doing with Events, Commands, and Message Bus.

**Pros**

Treating commands and events differently helps us understand which things have to succeed and which things we can tidy up later.

CreateBatch is definitely a less confusing name than BatchCreated. We are being explicit about the intent of our users, and explicit is better than implicit, right?

**Cons**

The semantic differences between commands and events can be subtle. Expect bikeshedding arguments over the differences.

We’re expressly inviting failure. We know that sometimes things will break, and we’re choosing to handle that by making the failures smaller and more isolated. This can make the system harder to reason about and requires better monitoring