# CQRS


| - | Read side | Write side |
| --- | --- | --- |
| Behavior | Simple read | Complex business logic |
| Cacheability | Highly cacheable | Uncacheable |
| Consistency | Can be stale | Must be transactionally consistent |

## Post/Redirect/Get and CQS

do it

## Updating a Read Model Table Using an Event Handler

