# Abstraction

## Mocks Versus Fakes

Here’s a short and somewhat simplistic definition of the difference between mocks and fakes:

Mocks are used to verify how something gets used; they have methods like assert_called_once_with(). They’re associated with London-school TDD.

Fakes are working implementations of the thing they’re replacing, but they’re designed for use only in tests. They wouldn’t work "in real life"; our in-memory repository is a good example. But you can use them to make assertions about the end state of a system rather than the behaviors along the way, so they’re associated with classic-style TDD.

We’re slightly conflating mocks with spies and fakes with stubs here, and you can read the long, correct answer in Martin Fowler’s classic essay on the subject called "Mocks Aren’t Stubs".

It also probably doesn’t help that the MagicMock objects provided by unittest.mock aren’t, strictly speaking, mocks; they’re spies, if anything. But they’re also often used as stubs or dummies. There, we promise we’re done with the test double terminology nitpicks now.

## Business Logic

Separate what we want to do from how to do it. 

Make our program output a list of commands that look like this:

```
("COPY", "sourcepath", "destpath"),
("MOVE", "old", "new"),
```





## Warp-up

We’ll see this idea come up again and again in the book: we can make our systems easier to test and maintain by simplifying the interface between our business logic and messy I/O.

Finding the right abstraction is tricky, but here are a few heuristics and questions to ask yourself:

* Can I choose a familiar Python data structure to represent the state of the messy system and then try to imagine a single function that can return that state?

* Separate the what from the how: can I use a data structure or DSL to represent the external effects I want to happen, independently of how I plan to make them happen?

* Where can I draw a line between my systems, where can I carve out a seam to stick that abstraction in?

* What is a sensible way of dividing things into components with different responsibilities? What implicit concepts can I make explicit?

* What are the dependencies, and what is the core business logic?

Practice makes less imperfect! And now back to our regular programming…​



