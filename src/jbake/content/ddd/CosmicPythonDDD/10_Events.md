# Events

- Avoiding Making a Mess:
    - Avoid Making a Mess of Our Web Controllers
    - Not Make a Mess of Our Model Either
    - Nor the Service Layer 

- Single Responsibility Principle
    - All Aboard the Message Bus
    - First, rather than being concerned about emails, our model will be in charge of recording events/facts about things that have happened. We’ll use a message bus to respond to events and invoke a new operation.
    - Events Are Simple Dataclasses
    - The Model Raises Events

- The Message Bus Maps Events to Handlers

- Option 1: The Service Layer Takes Events from the Model and Puts Them on the Message Bus
- Option 2: The Service Layer Raises Its Own Events (preferable)
- Option 3: The UoW Publishes Events to the Message Bus
- Option 4: Event bus pull events from UoW. See next chapter


## Warp-up

**Pros**	

A message bus gives us a nice way to separate responsibilities when we have to take multiple actions in response to a request.

Event handlers are nicely decoupled from the "core" application logic, making it easy to change their implementation later.

Domain events are a great way to model the real world, and we can use them as part of our business language when modeling with stakeholders.

**Cons**

The message bus is an additional thing to wrap your head around; the implementation in which the unit of work raises events for us is neat but also magic. It’s not obvious when we call commit that we’re also going to go and send email to people.

What’s more, that hidden event-handling code executes synchronously, meaning your service-layer function doesn’t finish until all the handlers for any events are finished. That could cause unexpected performance problems in your web endpoints (adding asynchronous processing is possible but makes things even more confusing).

More generally, event-driven workflows can be confusing because after things are split across a chain of multiple handlers, there is no single place in the system where you can understand how a request will be fulfilled.

You also open yourself up to the possibility of circular dependencies between your event handlers, and infinite loops.

## Domain Events and the Message Bus Recap

**Events can help with the single responsibility principle**

Code gets tangled up when we mix multiple concerns in one place. Events can help us to keep things tidy by separating primary use cases from secondary ones. We also use events for communicating between aggregates so that we don’t need to run long-running transactions that lock against multiple tables.

**A message bus routes messages to handlers**

You can think of a message bus as a dict that maps from events to their consumers. It doesn’t "know" anything about the meaning of events; it’s just a piece of dumb infrastructure for getting messages around the system.

**Option 1: Service layer raises events and passes them to message bus**

The simplest way to start using events in your system is to raise them from handlers by calling bus.handle(some_new_event) after you commit your unit of work.

**Option 2: Domain model raises events, service layer passes them to message bus**

The logic about when to raise an event really should live with the model, so we can improve our system’s design and testability by raising events from the domain model. It’s easy for our handlers to collect events off the model objects after commit and pass them to the bus.

**Option 3: UoW collects events from aggregates and passes them to message bus**

Adding bus.handle(aggregate.events) to every handler is annoying, so we can tidy up by making our unit of work responsible for raising events that were raised by loaded objects. This is the most complex design and might rely on ORM magic, but it’s clean and easy to use once it’s set up.


**Option 3: Event bus pull events from UoW**

See next chapter
