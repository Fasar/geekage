# Aggregate


* One Aggregate = One Repository
* Optimistic Concurrency with Version Numbers


**Definition:**

* constraint is a rule that restricts the possible states our model can get into
* invariant is defined as a condition that is always true.

**What:**

We want to protect the invariants of our system but allow for the greatest degree of concurrency. Maintaining our invariants inevitably means preventing concurrent writes; if multiple users can allocate DEADLY-SPOON at the same time, we run the risk of overallocating.

The Aggregate pattern is a design pattern from the DDD community that helps us to resolve this tension. An aggregate is just a domain object that contains other domain objects and lets us treat the whole collection as a single unit.

The only way to modify the objects inside the aggregate is to load the whole thing, and to call methods on the aggregate itself.

## Wrap-Up

**Pros**


* Python might not have "official" public and private methods, but we do have the underscores convention, because it’s often useful to try to indicate what’s for "internal" use and what’s for "outside code" to use. Choosing aggregates is just the next level up: it lets you decide which of your domain model classes are the public ones, and which aren’t.

* Modeling our operations around explicit consistency boundaries helps us avoid performance problems with our ORM.

* Putting the aggregate in sole charge of state changes to its subsidiary models makes the system easier to reason about, and makes it easier to control invariants.

**Cons**

* Yet another new concept for new developers to take on. Explaining entities versus value objects was already a mental load; now there’s a third type of domain model object?

* Sticking rigidly to the rule that we modify only one aggregate at a time is a big mental shift.

* Dealing with eventual consistency between aggregates can be complex.



## Aggregates and Consistency Boundaries Recap

**Aggregates are your entrypoints into the domain model**

By restricting the number of ways that things can be changed, we make the system easier to reason about.

**Aggregates are in charge of a consistency boundary**

An aggregate’s job is to be able to manage our business rules about invariants as they apply to a group of related objects. It’s the aggregate’s job to check that the objects within its remit are consistent with each other and with our rules, and to reject changes that would break the rules.

**Aggregates and concurrency issues go together**

When thinking about implementing these consistency checks, we end up thinking about transactions and locks. Choosing the right aggregate is about performance as well as conceptual organization of your domain.

