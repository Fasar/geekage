# Repository

## The Repository pattern

- Abstract the repository interface from the ORM
- Use params to pass session to Repository
- Implement Fake repo with inmemory + FakeSession 
- Implement BDD access

You have to provide docker-compose file to test the BDD Repository with a real database.

## Warp-up

**Apply dependency inversion to your ORM:**

Our domain model should be free of infrastructure concerns, so your ORM should import your model, and not the other way around.

**The Repository pattern is a simple abstraction around permanent storage:**

The repository gives you the illusion of a collection of in-memory objects. It makes it easy to create a FakeRepository for testing and to swap fundamental details of your infrastructure without disrupting your core application. See [appendix_csvs] for an example.