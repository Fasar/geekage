date=2023-01-17
type=page
tags=ddd
status=published
--------


# Pourquoi utiliser le Domain Driven Design

Le Domain Driven Design est un paradigme de conception logicielle qui permet de concevoir des applications en s'inspirant des modèles métiers. 
Il permet de concevoir des applications qui sont plus facilement maintenables et évolutives.

Le DDD nous aide à découper un domaine particulier en domaines de solutions indépendants.

Structurer ces contextes délimités en modules distincts au sein d'un monolithe ou microservice et l'utilisation d'événements de domaine pour communiquer entre eux nous aident à découpler l'application permettant une "vraie modularité".



## Avantages

Le DDD permet de concevoir des applications qui sont plus facilement maintenables et évolutives.
Les modèles métiers sont plus facilement compréhensibles par les développeurs et les utilisateurs, ce qui permet de réduire les risques d'erreurs et de mieux comprendre les besoins de l'application.

## Inconvénients

Le DDD peut être complexe à mettre en place et à comprendre pour les développeurs qui ne sont pas familiarisés avec ce paradigme.
Il peut également être difficile de définir les modèles métiers et de les faire évoluer au fur et à mesure que l'application évolue.