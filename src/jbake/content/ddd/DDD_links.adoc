# Liens pour le DDD
Fasar
2023-03-18
:jbake-type: page
:jbake-tags: project, myipproject
:jbake-status: draft

* DDD En Python:
** http://www.cosmicpython.com/book/preface.html

* Virtual ddd : https://virtualddd.com/
**  Organise des sessions pour le ddd
** Replay des sessions

* https://github.com/ddd-crew
** Ressources open sources
** Format d'atelier
** Outils, pratique

&nbsp;

* https://docs.axoniq.io/reference-guide/axon-server/introduction
** https://www.baeldung.com/axon-cqrs-event-sourcing
** Library and Book example: https://sgitario.github.io/axon-by-example/


&nbsp;

* Spring Boot and Event Sourcing: https://blog.knoldus.com/event-sourcing-with-springboot/
** Décrit comment créer un event store et le event repo.
** Attention, Le code n'est pas DDD
** Il n'y a aucun aggregate 


* https://www.baeldung.com/cqrs-event-sourcing-java

* Example simple d'une implementation de order management:
** https://github.com/evgeniy-khist/postgresql-event-sourcing


&nbsp;

* Example du event store pour PostgreSQL avec les regles pour ne pas faire deux fois un event avec le id+1
** https://github.com/mattbishop/sql-event-store/blob/master/postgres-event-store.ddl


* Event Store with PostgreSQL:
** https://github.com/evgeniy-khist/postgresql-event-sourcing#0cfc0523189294ac086e11c8e286ba2d

&nbsp;

* Blog sur le event store. Comment créer un event store avec une bdd relationnelle:
** https://softwaremill.com/implementing-event-sourcing-using-a-relational-database/


&nbsp;

- Event Sourcing from the Trenches: Mixed Feelings: https://www.continuousimprover.com/2016/06/event-sourcing-from-trenches-mixed.html
- The Ugly of Event Sourcing - Real-world Production Issues : https://www.linkedin.com/pulse/ugly-event-sourcing-real-world-production-issues-dennis-doomen/
- The Ugly of Event Sourcing - Projection Schema Changes: https://www.continuousimprover.com/2017/06/the-ugly-of-event-sourcing-projection.html
- In Event-sourcing how to deal with failure in production?: https://stackoverflow.com/questions/72112436/in-event-sourcing-how-to-deal-with-failure-in-production
- Mistakes and Recoveries When Building an Event Sourcing System : https://www.infoq.com/news/2019/07/mistakes-recovery-event-sourcing/

## A regarder sur Virtual DDD

- Patterns for the People - Kevlin Henney: https://virtualddd.com/sessions/1
- How To Read the Blue Book: Strategic Design with Mathias Verraes: https://virtualddd.com/sessions/7

- Introducing DDD to your Company with Barry O Sullivan: https://virtualddd.com/sessions/10

- [DDDDD-20] Aggregate Canvas by Kim Kao: https://virtualddd.com/sessions/26
- Bounded Contexts, Microservices, and Everything In Between: https://virtualddd.com/sessions/27
- Remote Bounded Context Modelling: https://virtualddd.com/sessions/29
- Context Maps - practically applied https://virtualddd.com/sessions/32
- Panel: Event Sourcing, Really with Alexey: https://virtualddd.com/sessions/33
- Functional Domain Modelling in Practice: https://virtualddd.com/sessions/35
- Exploring Techniques For Modelling Bounded Context Collaboration: https://virtualddd.com/sessions/52
- case-study context mapping with Michael Plöd: https://virtualddd.com/sessions/56
- Splitting systems towards bounded contexts and microservices: https://virtualddd.com/sessions/60
- Beyond the hexagonal architecture: Functional Core & ...: https://virtualddd.com/sessions/62
- From The Problem To Software - a Walkthrough with Krisztina Hirth: https://virtualddd.com/sessions/78
- Long term impact of architectural design decision: https://virtualddd.com/sessions/80
- https://virtualddd.com/sessions/
- https://virtualddd.com/sessions/


## Story:

- 1 Year of DDD: https://itnext.io/1-year-of-event-sourcing-and-cqrs-fb9033ccd1c6

## Source example:

- https://github.com/gpuget/katatrain

* How to implement Aggregates: https://github.com/MaibornWolff/aggregate-implementation-patterns-java
** With an example of managing aggregate in functional way.
** The Aggregate class contains all the logic and domain rules
** Aggregate state is immutable and containe the data.
** Regarder le code : https://github.com/MaibornWolff/aggregate-implementation-patterns-java/blob/challenge/src/main/java/domain/functional/traditional/customer/Customer2.java

