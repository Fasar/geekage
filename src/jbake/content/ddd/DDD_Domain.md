date=2023-01-18
type=page
tags=ddd
status=published
--------

# Le domaine dans le DDD

## Domain Richness


The Aggregate of any Bounded Context should be able to express the Business Language of the Bounded Context clearly.
Essentially, what it means in pure technical terms is that our Aggregate should not be anemic, that is, only containing getter/setter methods.

An anemic aggregate goes against the fundamental principle of DDD since it essentially would mean the Business Language being expressed in multiple layers of an application which in turn leads to an unmaintainable piece of software in the long run.

