date=2023-01-18
type=page
tags=ddd
status=published
--------


# Implementation d'un Event Store

## Introduction

Les Event Stores sont des systèmes permettant de stocker les événements d'un système distribué. 
Ils sont utilisés dans les architectures Event Sourcing et CQRS. 
Ils permettent d'enregistrer les événements générés par les différents composants d'un système distribué afin de pouvoir les

## Implementation avec un PostgreSQL

Nous allons implementer le stockage des événements dans une base de données PostgreSQL.


Vous pouvez retrouver les sources sur : https://github.com/fasar/sql-event-store

```sql
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS aggregateId_events;

CREATE TABLE aggregateId_events
(
    aggregateId TEXT NOT NULL,
    eventName  TEXT NOT NULL,
    PRIMARY KEY (aggregateId, eventName)
);

CREATE OR REPLACE RULE ignore_dup_aggregateId_events AS ON INSERT TO aggregateId_events
    WHERE EXISTS(SELECT 1
                 FROM aggregateId_events
                 WHERE (aggregateId, eventName) = (NEW.aggregateId, NEW.eventName))
    DO INSTEAD NOTHING;

-- immutable aggregateId_events
CREATE OR REPLACE RULE ignore_delete_aggregateId_events AS ON DELETE TO aggregateId_events
    DO INSTEAD NOTHING;

CREATE OR REPLACE RULE ignore_update_aggregateId_events AS ON UPDATE TO aggregateId_events
    DO INSTEAD NOTHING;



CREATE TABLE events
(
    aggregateId     TEXT        NOT NULL,
    aggregateKey  TEXT        NOT NULL,
    eventName      TEXT        NOT NULL,
    data       JSONB       NOT NULL,
    -- previous event uuid; null for first event; null does not trigger UNIQUE constraint
    previousSequence BIGINT UNIQUE,
    timestamp  TIMESTAMPT NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- ordering sequence
    sequence   BIGSERIAL PRIMARY KEY, -- sequence for all events in all entities
    FOREIGN KEY (aggregateId, eventName) REFERENCES aggregateId_events (aggregateId, eventName)
);

CREATE INDEX aggregateId_index ON events (aggregateKey, aggregateId);

-- immutable events
CREATE OR REPLACE RULE ignore_delete_events AS ON DELETE TO events
    DO INSTEAD NOTHING;

CREATE OR REPLACE RULE ignore_update_events AS ON UPDATE TO events
    DO INSTEAD NOTHING;


-- Can only use null previousSequence for first event in an aggregateId
CREATE OR REPLACE FUNCTION check_first_event_for_aggregateId() RETURNS trigger AS
$$
BEGIN
    IF (NEW.previousSequence IS NULL
        AND EXISTS (SELECT 1
                    FROM events
                    WHERE NEW.aggregateKey = aggregateKey
                      AND NEW.aggregateId = aggregateId))
    THEN
        RAISE EXCEPTION 'previousSequence can only be null for first aggregateId event';
END IF;
RETURN NEW;
END;
$$
    LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS first_event_for_aggregateId ON events;
CREATE TRIGGER first_event_for_aggregateId
    BEFORE INSERT
    ON events
    FOR EACH ROW
    EXECUTE PROCEDURE check_first_event_for_aggregateId();



-- previousSequence must be in the same aggregateId as the event
CREATE OR REPLACE FUNCTION check_previousSequence_in_same_aggregateId()
RETURNS trigger AS
$$
BEGIN
    IF (NEW.previousSequence IS NOT NULL
        AND NEW.previousSequence != (
                SELECT sequence FROM events
                    WHERE NEW.aggregateKey = aggregateKey
                        AND NEW.aggregateId = aggregateId
                    ORDER BY sequence DESC
                    LIMIT 1
             )
    )
    THEN
        RAISE EXCEPTION 'previousSequence must be the last entry of the event stream for the same aggregateId';
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS previousSequence_in_same_aggregateId ON events;
CREATE TRIGGER previousSequence_in_same_aggregateId
    BEFORE INSERT
    ON events
    FOR EACH ROW
    EXECUTE FUNCTION check_previousSequence_in_same_aggregateId();




truncate events;
truncate aggregateId_events cascade;
ALTER SEQUENCE events_sequence_seq RESTART WITH 1;
```




## Implementation avec Spring Boot

Nous allons utiliser Spring Boot pour créer un microservice qui va permettre de stocker les événements dans une base de données PostgreSQL.
Nous allons utiliser le framework Spring Data pour accéder à la base de données.


Les Events doivent être modélisé dans un flux de données.
Ils possèdent un identifiant unique, une date de création, un type et un contenu.
Le contenu est un objet JSON.
L'identifiant unique est généré par le le moteur PostgreSQL.

### Event

```java
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventWrite {
    String aggregateId;          // Name of the aggregate
    String aggregateKey;       // Id of the event of the aggregate
    String eventName;           // Name of the event
    JSon data;              // Content of the event
    long previousSequence; // Id of the previous event
}
```

```java
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventRead {
    long sequence;          // Id of the event
    Instant timestamp;      // Date of the event
    String aggregateId;          // Name of the aggregate
    String aggregateKey;       // Id of the event of the aggregate
    String eventName;           // Name of the event
    JSon data;              // Content of the event
    long previousSequence; // Id of the previous event
}
```


### R2DBC Repository

```java
@Repository
public interface ReadEventRepository extends ReactiveCrudRepository<EventRead, String> {

    Flux<EventRead> findAllByAggregateIdAndAggregateKey(String aggregateId, String aggregateKey);
}
```

```java
@Repository
public interface WriteEventRepository extends ReactiveCrudRepository<EventWrite, String> {
 // No need to implement anything
}
```




### application.properties

```properties
spring.r2dbc.url=r2dbc:postgresql
spring.r2dbc.username=postgres
spring.r2dbc.password=postgres
spring.r2dbc.schema=classpath:schema.sql
spring.r2dbc.initialization-mode=always
```

