date=2023-01-18
type=page
tags=ddd
status=published
--------

# Technical things


## Entity and Value Objects

Entity Objects within a Bounded Context have an identity of their own but always exist within a root aggregate, that is, they cannot exist independently, and
they never change during the complete lifecycle of the aggregate. 

Value Objects on the other hand have no identity of their own and are easily replaceable in any instance of an aggregate.


## Command Handler

Every Command will have a corresponding Command Handler.
The purpose of the Command Handler is to process the input command and set the state of the Aggregate.

Command Handlers are the only place within the Domain Model where Aggregate state is set.
This is a strict rule that needs to be followed to help implement a rich Domain Model.

Methodology of implementation will be to identify the routines/functions on the Aggregates which can be denoted as Command Handlers.
So Command Handlers are implemented as a function or a constructor.


## Queries

Queries within the Bounded Context are responsible for providing the state of the Bounded Context’s Aggregate to external consumers.

## Events

An event within a Bounded Context is any operation that publishes the Bounded Context’s Aggregate State Changes as Events. 
Since Commands change the state of an Aggregate, it is safe to assume that any Command operation within a Bounded Context will result in a corresponding Event. 
The subscribers of these events could be either other Bounded Contexts within the same domain or Bounded Contexts belonging to any other external domains.

Domain Events play a central role within a microservices architecture, and it is important to implement them in a robust manner. 
The distributed nature of a microservices architecture mandates the usage of Events via a choreography mechanism to maintain state and transactional consistency between the various Bounded Contexts of a microservices-based application.



There are four stages to the implementation of a robust event-driven choreography
architecture:

- Register the Domain Events that need to be raised from a Bounded Context.
- Raise the Domain Events that need to be published from a Bounded Context.
- Publish the Events that are raised from a Bounded Context.
- Subscribe to the Events that have been published from other Bounded Contexts.

Considering the capabilities provided by the MicroProfile platform, the implementation is split across multiple areas:

- Raising of Events is implemented by the Application Services.
- Publishing of Events is implemented by the Outbound Services.
- Subscribing to Events is handled by the Interface/Inbound services.


## Domain Model Services

There are three types of Domain Model Services for any Bounded Context:

- Inbound Services where we implement well-defined interfaces which enable external parties to interact with the Domain Model
- Outbound Services where we implement all interactions with External Repositories/other Bounded Contexts
- Application Services which act as the façade layer between the Domain Model and both Inbound and Outbound services

![](images/techthing_domail-model-services.png)