date=2023-01-18
type=page
tags=ddd
status=published
--------

# Séparation des préoccupations avec DDD


Source principale: Practical Domain Driven Design in Entreprise Java by Vijay Nair


![](images/separation_concerns.png)




## Interface

This package contains all the possible inbound services a Bounded Context provides
classified by protocol.

They serve two main purposes:

- Protocol negotiation on behalf of the domain model (e.g., REST
API(s), Web API(s), WebSocket(s), FTP(s))
- View adapters for data (e.g., Browser View(s), Mobile View(s))


![](images/sepconcerns_Interface.png)

- domain-name.interface
    - web
        - viewadapter
    - rest
        - viewadapter
    - socket
        - viewadapter
    - eventhandlers
    - file (for batch processing)


## Application

This package contains the Application services a Bounded Context’s domain model would require.

Application services classes serve multiple purposes:

- Act as ports for input interfaces and output repositories
- Commands, Queries, Events, and Saga participants
- Transaction initiation, control, and termination
- Centralized concerns (e.g., Logging, Security, Metrics) for the underlying domain model
- Data transfer object transformation
- Callouts to other Bounded Contexts


Application services act as the façade for the Bounded Context’s Domain Model.
They provide façade services to dispatch Commands/Queries to the underlying Domain Model.
They are also the place where we place outbound calls to other Bounded Contexts as part of the processing of a Command/Query.

To summarize, Application Services:

- Participate in Command and Query Dispatching.
- Invoke infrastructural components where necessary as part of the Command/Query processing.
- Provide Centralized concerns (e.g., Logging, Security, Metrics) for the underlying Domain Model.
- Make callouts to other Bounded Contexts.



- domain-name.application
    - transform
    - sagas
    - internal
        - commands
        - queries
        - events
        - commandservice
        - queryservice
        - outboundservice
    - external
        - commands
        - queries
        - events

## Domain

This package contains the Bounded Context’s domain model.


This package contains the Bounded Context’s Domain Model.
This is the heart of the Bounded Context’s Domain Model which contains the implementation of the core Business Logic.


The following are the core classes of our Bounded Contexts:

- Aggregates
- Entities
- Value Objects
- Domain Rules

![](images/sepconcerns_domain.png)

- domain-name.domain
    - aggregates
    - entities
    - valueobjects
    - rules
    - commands
    - events

## Infrastructure

The infrastructure package serves four main purposes:

- When a Bounded Context receives an operation related to its state (Change of State, Retrieval of State), it needs an underlying repository to process the operation; in our case, this repository is our MySQL Database instance(s). 
    The infrastructure package contains all the necessary components required by the Bounded Context to communicate to the underlying repository. 
    As part of our implementation, we intend to use either JPA or JDBC to implement these components.
- When a Bounded Context needs to communicate a state change event, it needs an underlying Event Infrastructure to publish the state change event. 
  In our implementation, we intend to use a message broker as the underlying Event Infrastructure (RabbitMQ).
  The infrastructure package contains all the necessary components required by the Bounded Context to communicate to the underlying message broker.
- When a Bounded Context needs to communicate with another Bounded Context synchronously, it needs an underlying infrastructure to support a service-to-service communication via REST.
  The infrastructure package contains all the necessary components required by the Bounded Context to communicate to other Bounded Contexts.
- The final aspect that we include in the infrastructural layer is an kind of MicroProfile-specific configuration.


- domain-name.infrastructure
    - configuration
    - repositories
        - jpa
        - jdbc
    - broker
        - rabbitmq
    - services
        - http




# Shared Kernels

Sometimes the domain model may need to be shared across multiple Bounded Contexts.
Shared kernels within DDD offer us a robust mechanism to share domain models reducing the amount of duplicated code. 
Shared kernels are easier to implement within a monolith rather than a microservices-based application which advocates a much higher level of independency.


It does come up with a fair degree of challenges though as multiple teams need to agree on what aspect of the domain model would need to be shared across Bounded Contexts.

![](images/sepconcerns_shared-domain.png)

- shared.infrastructure
    - events
        - cdi
    - configuration
    - repositories
        - jpa
        - jdbc
    - broker
        - rabbitmq
    - services
        - http  
