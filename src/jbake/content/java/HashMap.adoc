# Description des HashMap
fasar
2020-02-11
:jbake-type: post
:jbake-tags: java
:jbake-status: published
:idprefix:

Les Maps, une notion primordiale dans le développement.

Du point de vue de la théorie des ensemble, les Maps sont une application link:https://fr.wikipedia.org/wiki/Application_(math%C3%A9matiques)[(lien wikipedia)].
Elles permettent la définition de relations entre deux ensembles d'objets.
La fonction `put` permet de définir ses relations et la fonction `get` permet de retrouver l'image d'une cléf passée en parametre.

Java dispose de pas mal d'objet pour représenter les Maps.
La plus utilisée est probablement la link:https://docs.oracle.com/javase/8/docs/api/index.html?java/util/HashMap.html[HashMap].

NOTE: En Java, on utilise les HashMap, mais sait-on vraiment ce qu'il y a derriére ?

## Model de donnée des Hash Map

Une explication théorique est disponible sur link:https://fr.wikipedia.org/wiki/Table_de_hachage[wikipedia].

Pour l'explication du model de donnée, nous allons utilisé VisualVM.
C'est un outil qui permet d'explorer les applications Java : les variables, les threads, la mémoire ...

L'application de test est donné sur cette page : link:HashMapApp.html[lien].

Dans le graphique ci-dessous, il est claire que les nœuds HashMaps$Node dispose d'un hash, d'une clef et d'une valeur.
De plus, nous voyons que l'objet dispose d'un champ next qui fait réference aux nœuds suivant en cas de conflit de valeur de hash. 

.VisualVM Memory dump - Visualisation d'une hash map
image:images/visualvm.jpg[]


== Performance

On peut se demander la performance d'une hashmap comparé a une recherche dans une list.
La comparaison d'une string est beaucoup plus longue que la comparaison d'un long (le hash).

Pour être certain, les résultat du code sur cette link:HashMapApp.html[page] sont les suivants:

.Resultat du test
----
Map Finding  [element1] in 5 ms
Map Finding  [element2] in 6 ms
Map Finding  [element3] in 6 ms
Map Finding  [element4] in 6 ms
Map Finding  [element5] in 5 ms
Map Finding  [element6] in 6 ms
List Finding [element1] in 8 ms
List Finding [element2] in 13 ms
List Finding [element3] in 20 ms
List Finding [element4] in 26 ms
List Finding [element5] in 32 ms
List Finding [element6] in 37 ms
----