# OptaPlanner Terminology
fasar
2024-04-05
:jbake-type: post
:jbake-tags: java,OptaPlanner,projet
:jbake-status: draft
:idprefix:


Source: https://docs.optaplanner.org/9.44.0.Final/optaplanner-docs/html_single/index.html#springBootJavaQuickStart

## Terminology

- Une entité @PlanningSolution est une classe qui contient toutes les entités et variables de planification.
- Elle modélise les "problem facts" ou @ProblemFactCollectionProperty
- Elle décrit les @ValueRangeProvider pour chaque variable de planification, c'est-à-dire les valeurs possibles pour chaque variable de planification.
-  @ValueRangeProvider  et @ProblemFactCollectionProperty sont souvent ensemble.



## Classes and annotations


- @PlanningSolution: A class that contains all the planning entities and planning variables. 
- @PlanningEntity: A class that is considered for optimization. It is a JavaBean with a getter and a setter for each planning variable.
- @PlanningVariable: A property of a @PlanningEntity that changes during planning. It is a JavaBean with a getter and a setter.


- planningValueRangeProvider: A method that returns a Collection of all possible values for a planning variable. This is used to generate the initial solution and to generate moves.

- EasyScoreCalculator: A class that calculates the score of a solution.
- ConstraintProvider: A class that defines the constraints of the problem and helps to calculate the score of a solution.


## Example

- Lesson : @PlanningEntity
    - Timeslot : @PlanningVariable
    - Room : @PlanningVariable

- TimeTableEasyScoreCalculator implements EasyScoreCalculator<TimeTable, HardSoftScore>
- or : TimeTableConstraintProvider implements ConstraintProvider

- TimeTable : @PlanningSolution
    - List<Timeslot> timeslotList : @ValueRangeProvider and @ProblemFactCollectionProperty
    - List<Room> roomList : @ValueRangeProvider and @ProblemFactCollectionProperty
    - List<Lesson> lessonList : @PlanningEntityCollectionProperty
    - HardSoftScore score : @PlanningScore


    