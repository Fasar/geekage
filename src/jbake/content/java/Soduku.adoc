= Soduku Resolver
fasar
2022-08-31
:jbake-type: post
:jbake-tags: java
:jbake-status: published
:idprefix:


Soduku resolver est un bon exercise pour la programmation récursivle

Le Sudoku est un jeu de puzzle qui consiste à résoudre un puzzle de 9x9 cases.
La résolution est réalisée par une recherche en profondeur.


Inspiré de la source : https://github.com/thierryler/article-sudoku/    

## Definir les règles

[source,java]
----
  /**
   * Indique si une valeur peut etre utilisee dans le tableau à la position
   * indiquée
   * 
   * @param value
   * @param rowIndex
   * @param columnIndex
   * @param board
   * @return
   */
  default boolean isCorrectAt(final char value, final int rowIndex, final int columnIndex, final char[][] board) {
    return !isValueInRow(value, rowIndex, board) // Pas sur la ligne
        && !isValueInColumn(value, columnIndex, board) // Pas sur la colonne
        && !isValueInBlock(value, rowIndex, columnIndex, board); // Pas sur le bloc
  }

  /**
   * Indique si la valeur est deja dans la ligne.
   * 
   * @param value
   * @param board
   * @param rowIndex
   * @return
   */
  default boolean isValueInRow(final char value, final int rowIndex, final char[][] board) {
    for (int j = 0; j < 9; j++) {
      if (board[rowIndex][j] == value) {
        return true;
      }
    }
    return false;
  }

  /**
   * Indique si la valeur est deja dans la colonne.
   * 
   * @param value
   * @param board
   * @param columnIndex
   * @return
   */
  default boolean isValueInColumn(final char value, final int columnIndex, final char[][] board) {
    for (int i = 0; i < 9; i++) {
      if (board[i][columnIndex] == value) {
        return true;
      }
    }
    return false;
  }

  /**
   * Indique si la valeur est deja dans le bloc.
   * 
   * @param value
   * @param board
   * @param rowIndex
   * @param columnIndex
   * @return
   */
  default boolean isValueInBlock(final char value, final int rowIndex, final int columnIndex, final char[][] board) {
    final int blockTopRowIndex = rowIndex - rowIndex % 3;
    final int blockLeftColumnIndex = columnIndex - columnIndex % 3;

    for (int i = blockTopRowIndex; i < blockTopRowIndex + 3; i++) {
      for (int j = blockLeftColumnIndex; j < blockLeftColumnIndex + 3; j++) {
        if (board[i][j] == value) {
          return true;
        }
      }
    }
    return false;
  }

----



## Des outils

[source,java]
----
  // Les caractére possibles
  char[] POSSIBLE_CHARACTERS = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };

  /**
   * Définition d'une fonction de calcule de la solution pour le tableau indiqué.
   * 
   * 
   * @param board
   * @return
   * @throws NotResolvableException si le tableau n'a pas de solution.
   */
  char[][] solve(final char[][] board) throws NotResolvableException;


  /**
   * Fait une copie du tableau.
   * 
   * @param board
   * @return
   */
  char[][] copyOfBoard(final char[][] board) {
    final char[][] copy = new char[9][9];
    for (int i = 0; i < 9; i++) {
      for (int j = 0; j < 9; j++) {
        copy[i][j] = board[i][j];
      }
    }
    return copy;
  }


  /**
   * Affiche le tableau dans la console.
   * 
   * @param board
   */
  void printBoard(final char[][] board) {
    for (int i = 0; i < 9; i++) {
      if (i % 3 == 0) {
        System.out.println("-------------");
      }
      for (int j = 0; j < 9; j++) {
        if (j % 3 == 0) {
          System.out.print('|');
        }
        System.out.print(board[i][j]);
      }
      System.out.println("|");
    }
    System.out.println("-------------");
  }

----


## La fonction main

[source,java]
----
public class Main {

  public static void main(String[] args) {
    System.out.println("SUDOKU SOLVER");

    final char[][] board = { //
        { '5', '3', ' ', ' ', '7', ' ', ' ', ' ', ' ' }, //
        { '6', ' ', ' ', '1', '9', '5', ' ', ' ', ' ' }, //
        { ' ', '9', '8', ' ', ' ', ' ', ' ', '6', ' ' }, //
        { '8', ' ', ' ', ' ', '6', ' ', ' ', ' ', '3' }, //
        { '4', ' ', ' ', '8', ' ', '3', ' ', ' ', '1' }, //
        { '7', ' ', ' ', ' ', '2', ' ', ' ', ' ', '6' }, //
        { ' ', '6', ' ', ' ', ' ', ' ', '2', '8', ' ' }, //
        { ' ', ' ', ' ', '4', '1', '9', ' ', ' ', '5' }, //
        { ' ', ' ', ' ', ' ', '8', ' ', ' ', '7', '9' }, //
    };

    final SudokuSolver solver = new RecursiveSudokuSolver();

    System.out.println("Tableau de depart :");
    solver.printBoard(board);

    try {
      final char[][] solvedBoard = solver.solve(board);
      System.out.println();
      System.out.println("Tableau de solution :");
      solver.printBoard(solvedBoard);
    } catch (NotResolvableException e) {
      System.out.println("This board can not be solved!");
    }

  }
}
----




