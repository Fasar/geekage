# Podman sur Windows avec WSL2
Fasar
2021-11-22
:jbake-type: post
:jbake-tags: docker,windows
:jbake-status: draft

## Installer WSL

Installer WSL si ce n'est pas deja fait: https://docs.microsoft.com/en-us/windows/wsl/install

## Installer Podma

Source: https://oldgitops.medium.com/setting-up-podman-on-wsl2-in-windows-10-be2991c2d443

Check la version de linux sur windows : `wsl -l -v`

[source,bash]
----
C:> wsl -l -v
  NAME      STATE           VERSION
* Ubuntu    Running         1
----

Si VERSION est a 1, il faut le convertir en 2 : `wsl --set-default-version 2`


