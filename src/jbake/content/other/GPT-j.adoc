= Run GPT-J-6B on your own machine
fasar
2023-01-31
:jbake-type: posts
:jbake-tags: others
:jbake-status: draft
:idprefix:


- https://www.altamira.ai/blog/open-source-gpt-alternative-solutions/
- https://github.com/EleutherAI/gpt-neox/
- https://arankomatsuzaki.wordpress.com/2021/06/04/gpt-j/   
- https://towardsdatascience.com/how-to-build-your-own-gpt-j-playground-733f4f1246e5
- https://towardsdatascience.com/how-you-can-use-gpt-j-9c4299dd8526
- https://huggingface.co/blog/gptj-sagemaker


## CodeGen

- https://github.com/salesforce/CodeGen

