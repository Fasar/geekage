# LDAP - Quelques informations de base
fasar
2023-10-26
:jbake-type: posts
:jbake-tags: others
:jbake-status: published
:idprefix:

## Introduction

LDAP est un protocole de communication client-serveur utilisé pour accéder à des services d'annuaire.
Les annuaires LDAP sont des bases de données hiérarchiques qui stockent des informations sur des objets tels que des utilisateurs, des groupes, des ordinateurs, des imprimantes, etc.

LDAP est souvent utilisé pour l'authentification et l'autorisation dans les réseaux d'entreprise, les systèmes de messagerie, les applications web et d'autres systèmes informatiques. 
Les clients LDAP peuvent interroger les serveurs LDAP pour récupérer des informations sur les objets stockés dans l'annuaire, tels que les noms d'utilisateur, les adresses e-mail, les numéros de téléphone, les groupes d'utilisateurs, etc.

LDAP est un protocole ouvert et standardisé qui est largement utilisé dans les environnements d'entreprise. 
Il est souvent utilisé en conjonction avec d'autres protocoles tels que Kerberos pour fournir une sécurité supplémentaire lors de l'authentification et de l'autorisation des utilisateurs.

## Terminologie

Dans un arbre LDAP, la terminologie employée est la suivante :

- **Distinguished Name (DN)** : Le DN est une chaîne de caractères qui identifie de manière unique un objet dans l'arbre LDAP. Il est composé d'une séquence de RDNs (Relative Distinguished Names) qui décrivent l'emplacement de l'objet dans l'arbre.

- **Relative Distinguished Name (RDN)** : Le RDN est un élément du DN qui décrit l'emplacement d'un objet dans l'arbre LDAP. Il est composé d'un attribut et d'une valeur, séparés par un signe égal (=).

- **Attribute** : Un attribut est une propriété d'un objet dans l'arbre LDAP. Il est composé d'un nom et d'une ou plusieurs valeurs. Les attributs peuvent être standard (définis par le protocole LDAP) ou personnalisés (définis par l'utilisateur).

- **Object Class** : Une classe d'objet est un ensemble d'attributs qui définissent les propriétés d'un objet dans l'arbre LDAP. Les classes d'objet peuvent être standard (définies par le protocole LDAP) ou personnalisées (définies par l'utilisateur).

- **Entry** : Une entrée est un objet dans l'arbre LDAP qui est identifié par un DN unique. Elle est composée d'un ensemble d'attributs qui définissent les propriétés de l'objet.

- **Root DSE** : Le Root DSE (Directory System Entry) est une entrée spéciale dans l'arbre LDAP qui contient des informations sur le serveur LDAP, telles que le nom du serveur, la version du protocole LDAP, les capacités du serveur, etc.


## Organisation des données

Les données dans un annuaire LDAP sont organisées en entrées.
Chaque entrée est un objet qui contient un ensemble d'attributs.
Les attributs sont des paires nom-valeur qui décrivent une caractéristique de l'objet.



Les entrées sont organisées dans une structure hiérarchique appelée DIT (Directory Information Tree).
La racine de l'arbre est appelée base de l'annuaire et est identifiée par un DN (Distinguished Name) unique.
Chaque entrée dans l'annuaire a un DN unique qui identifie son emplacement dans l'arbre.
Le DN d'une entrée est composé de tous les RDN (Relative Distinguished Name) de l'entrée, en commençant par l'entrée elle-même et en remontant jusqu'à la racine de l'arbre.

Par example, l'entrée suivante a un DN de `cn=John Doe,ou=users,dc=example,dc=com` :

```
dn: cn=John Doe,ou=users,dc=example,dc=com
cn: John Doe
givenName: John
sn: Doe
uid: jdoe
mail:
    - john.do@example.com
    - jd@example.com
```

L'entrée ci-dessus a un RDN de `cn=John Doe` et est située dans l'arbre sous `ou=users,dc=example,dc=com`.
L'entrée a quatre attributs : `cn`, `givenName`, `sn` et `uid`.
L'attribut `mail` est multivalué, ce qui signifie qu'il peut avoir plusieurs valeurs.

Un arbre typique LDAP ressemble à ceci :

```
dc=example,dc=com
├── ou=users
│   ├── cn=John Doe
│   ├── cn=Jane Doe
│   └── cn=Bob Smith
└── ou=groups
    ├── cn=admins
    └── cn=developers
```

L'adresse d'un nœud dans l'arbre LDAP est appelée DN (Distinguished Name).
Le DN d'une entrée est composé de tous les RDN (Relative Distinguished Name) de l'entrée, en commençant par l'entrée elle-même et en remontant jusqu'à la racine de l'arbre.



### Les attributs 

Il existe de nombreux attributs LDAP qui peuvent être utilisés pour décrire les objets dans un annuaire LDAP. Voici quelques exemples d'attributs couramment utilisés :

- `dc` (Domain Component) : Le composant de domaine de l'objet.
- `cn` (Common Name) : Le nom commun de l'objet.
- `ou` (Organizational Unit) : L'unité organisationnelle à laquelle appartient l'objet.
- `uid` (User ID) : L'identifiant de l'utilisateur.
- `mail` (Email Address) : L'adresse e-mail de l'utilisateur.
- `givenName` (First Name) : Le prénom de l'utilisateur.
- `sn` (Surname) : Le nom de famille de l'utilisateur.
- `memberOf` (Group Membership) : Les groupes auxquels l'utilisateur appartient.
- `userPassword` (User Password) : Le mot de passe de l'utilisateur (généralement stocké sous forme de hachage).
- `telephoneNumber` (Telephone Number) : Le numéro de téléphone de l'utilisateur.
- `description` (Description) : Une description de l'objet.


Les attributs LDAP utilisés dans une entreprise dépendent de l'application ou du système qui utilise l'annuaire LDAP. Par exemple, un système de messagerie peut utiliser l'attribut `mail` pour stocker les adresses e-mail des utilisateurs, tandis qu'un système de gestion des identités peut utiliser l'attribut `memberOf` pour stocker les groupes auxquels les utilisateurs appartiennent.


Active Directory est une implémentation de l'annuaire LDAP utilisée par les systèmes d'exploitation Windows. Voici quelques-uns des attributs spécifiques à Active Directory :

- `sAMAccountName` : Le nom de compte de sécurité de l'utilisateur.
- `userPrincipalName` : Le nom principal de l'utilisateur (souvent utilisé pour l'authentification).
- `objectSid` : L'identificateur de sécurité de l'objet.
- `objectGUID` : L'identificateur unique de l'objet.
- `distinguishedName` : Le DN de l'objet.
- `memberOf` : Les groupes auxquels l'utilisateur appartient.
- `userAccountControl` : Les paramètres de contrôle de compte de l'utilisateur (par exemple, si le compte est désactivé ou verrouillé).
- `pwdLastSet` : La date et l'heure de la dernière modification du mot de passe de l'utilisateur.
- `lastLogon` : La date et l'heure de la dernière connexion de l'utilisateur.
- `whenCreated` : La date et l'heure de la création de l'objet.
- `whenChanged` : La date et l'heure de la dernière modification de l'objet.

Ces attributs sont utilisés pour stocker des informations spécifiques à Active Directory, telles que les noms de compte de sécurité, les identificateurs de sécurité, les paramètres de contrôle de compte, les dates de création et de modification, etc.

