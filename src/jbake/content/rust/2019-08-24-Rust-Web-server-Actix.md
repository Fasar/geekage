type=page
title=Rust Web Server with Actix
date=2019-08-31
categories=rust http server
status=published
~~~~~~


# Rust web server

- It apear Actix is the most used web server
- https://actix.rs/docs/installation/


- Compiling a simple server HTTP with static lib is 80Mo in debug

```rust
    #[macro_use]
    extern crate actix_web;

    use actix_files as fs;
    use actix_web::{web, App, HttpResponse, HttpServer, Responder};

    use handlebars::Handlebars;


    #[macro_use]
    extern crate serde_json;

    fn say_hello() -> impl Responder {
        HttpResponse::Ok().body("Hello world!")
    }

    fn say_hello2() -> impl Responder {
        HttpResponse::Ok().body("Hello world again!")
    }

    // Macro documentation can be found in the actix_web_codegen crate
    #[get("/")]
    fn index(hb: web::Data<Handlebars>) -> HttpResponse {
        let data = json!({
            "name": "Handlebars"
        });
        let body = hb.render("index", &data).unwrap();

        HttpResponse::Ok().body(body)
    }

    #[get("/{user}/{data}")]
    fn user(hb: web::Data<Handlebars>, info: web::Path<(String, String)>) -> HttpResponse {
        let data = json!({
            "user": info.0,
            "data": info.1
        });
        let body = hb.render("user", &data).unwrap();

        HttpResponse::Ok().body(body)
    }



    fn main() {
        // Handlebars uses a repository for the compiled templates. This object must be
        // shared between the application threads, and is therefore passed to the
        // Application Builder as an atomic reference-counted pointer.
        let mut handlebars = Handlebars::new();
        handlebars
            .register_templates_directory(".html", "./static/templates")
            .unwrap();
        let handlebars_ref = web::Data::new(handlebars);


        HttpServer::new(move || {
            App::new()
                .register_data(handlebars_ref.clone())
                .service(index)
                .service(user)
                .route("/hello", web::get().to(say_hello))
                .route("/hello2", web::get().to(say_hello2))
                .service(fs::Files::new("/", "/static").show_files_listing())
        })
        .bind("127.0.0.1:8088")
        .unwrap()
        .run()
        .unwrap();
    }
```