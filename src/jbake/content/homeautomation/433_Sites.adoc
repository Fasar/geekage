# 433 Mhz - Sites
fasar
2021-01-03
:jbake-type: post
:jbake-tags: electricité
:jbake-status: published
:idprefix:

* Brancher du 433 Mhz sur rasperry pi:
    * https://devotics.fr/jouer-avec-du-433mhz-sur-home-assistant/
    * Utilise 433Utils.git
    * Add-on de openHA:  Raspberry Pi RC Switch Binding
    * OU https://www.raspberrypi.org/forums/viewtopic.php?f=37&t=66946
    * https://hackaday.com/2018/08/29/raspberry-pi-as-433-mhz-to-mqtt-gateway/
    * https://www.pofilo.fr/post/20190529-home-assistant-433mhz/


* ESP8266 MQTT to 433 Mhz
    * https://github.com/dbudwin/RoboHome-ESP8266
    * https://github.com/puuu/MQTT433gateway
    * https://1technophile.blogspot.com/2016/09/433tomqttto433-bidirectional-esp8266.html
    * https://www.markus-haack.com/mqtt-433mhz-gateway-homie avec la def mqtt-homie https://github.com/homieiot/homie-esp8266 compatible avec openHAB https://www.openhab.org/addons/bindings/mqtt.homie/
     


* Debug 433
    * Sniffer du 433 : https://github.com/ninjablocks/433Utils
    * Envoyer des trames 433 avec un arduino : https://github.com/sui77/rc-switch
    * Envoyer des trames 433 avec rpi : https://github.com/r10r/rcswitch-pi

* Commander des devices 433 RFLink:
    * http://www.rflink.nl/blog2/

image::2-12-2020-07-49-36-AM.png[]

* Bridge MQTT TO 433: 
    * https://blogwifi.fr/sonoff-rf-bridge-une-passerelle-radio-mqtt-a-10-e/
    * Github : https://github.com/1technophile/OpenMQTTGateway
    * https://docs.openmqttgateway.com/


## Comprendre la transmition radio

http://blog.idleman.fr/raspberry-pi-10-commander-le-raspberry-pi-par-radio/


## 433 Mhz sur Raspi

* source : https://www.pofilo.fr/post/20190529-home-assistant-433mhz/
* source : https://wifsimster.github.io/2014/02/25/Controle-de-prise-Chacon-DIO-First-via-RaspberryPi/

* GPIO Raspi: https://www.raspberrypi.org/documentation/usage/gpio/

* RPi 433 et DIO
    * https://www.pofilo.fr/post/20210131-chacon-dio-433mhz/
    * http://blog.idleman.fr/raspberry-pi-10-commander-le-raspberry-pi-par-radio/


## Commander du Rf avec RPi


* https://opensource.com/article/19/3/physical-computing-rust-raspberry-pi
** https://gpiozero.readthedocs.io/en/stable/#
* https://docs.rs/wiringpi/0.2.4/wiringpi/pin/struct.InputPin.html

## Linux Temp Réel

* https://linuxembedded.fr/2019/09/le-temps-reel-sous-linux